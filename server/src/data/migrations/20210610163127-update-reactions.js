'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     queryInterface.addColumn(
      'postReactions',
      'isDislike',
      {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: false
      }
       ), {transaction}
      ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('postReactions', { transaction })
    ]))
  
};
