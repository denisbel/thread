'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
  .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
  .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
  /**
   * Add altering commands here.
   *
   * Example:
   * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
   */
   queryInterface.addColumn(
    'posts',
    'deletedAt',
    {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: null
    }
     ), {transaction}
    ]))),

down: queryInterface => queryInterface.sequelize
  .transaction(transaction => Promise.all([
    queryInterface.dropTable('posts', { transaction })
  ]))

};
