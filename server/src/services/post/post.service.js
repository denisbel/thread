class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  deletePostById(id){
    this._postRepository.deleteById(id);
    return this._postRepository.getPosts({ count: 10, from: 0, hideOwn: false, showOwn: false });
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async setReaction(userId, { postId, isLike, isDislike}) {
    if (isLike === undefined) isLike = !isDislike;
    if (isDislike === undefined) isDislike = !isLike;
    const updateOrDelete = react => {
      if (react.isLike === isLike)
        return this._postReactionRepository.deleteById(react.id);
      else return this._postReactionRepository.updateById(react.id, {
        isLike,
        isDislike
      });
      if (react.isDislike === isDislike)
        return this._postReactionRepository.deleteById(react.id);
      else return this._postReactionRepository.updateById(react.id, {
        isDislike,
        isDislike
      });
    }


    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const likeReduce = isDislike && reaction!=null && reaction.isLike ? -1 : 0;
    const dislikeReduce = isLike && reaction!=null && reaction.isDislike ? -1 : 0;
    
    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike, isDislike });

    // the result is an integer when an entity is deleted
    const response =  Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
    if(response.then) { //check if response is promise or not
      response.then(result => {result.dataValues.likeReduce=likeReduce; return result}).then(result => {result.dataValues.dislikeReduce=dislikeReduce; return result});
    }
    else {
    response.likeReduce = likeReduce;
    response.dislikeReduce = dislikeReduce;
    }
    return response;
  }
}

export { Post };
